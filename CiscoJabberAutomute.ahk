﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.


windowOn := false

Loop 
{
    ; find teh jabber conference window
    id := GetConferenceWindow()
    ; if we found it and haven't muted it yet, do so
    if (id != "" and windowOn == false)
    {
        WinGet, active_id, ID, A
        WinActivate, ahk_id %id%
        ; toggle mute
        send, {Control Down}{Down}{Control Up}
        ; remember we muted it already
        windowOn := True
        WinActivate, ahk_id %active_id%
    }
    ; if we can't find the window, and it's been muted, reset our state
    if (id == "" and windowOn)
    {
        windowOn :=False
    }
    Sleep 5000
}


GetConferenceWindow() 
{
    WinGet, WinID, ID, Conference
    return WinID
}
