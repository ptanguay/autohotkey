#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn  ; Enable warnings to assist with detecting common errors.
#SingleInstance force

;SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.


; put pc to sleep with Windows-s
<#s::
	DllCall("PowrProf\SetSuspendState", "int", 0, "int", 1, "int", 0)
Return

; remap capslock to control
; SetCapsLockState , Off
; Capslock::Ctrl

; send :yes: with Alt-y
!y::
	WinGetActiveTitle, currentApplication
; 	MsgBox, % currentApplication
	if (InStr(currentApplication,"Slack")) {
		send, :{+}1:
		return
	}
	send, :yes:
Return

; send my zoomurl
::zoomurl::https://oracle.zoom.us/my/phil.tanguay
::myzoom::https://oracle.zoom.us/my/phil.tanguay

; toggle the mute on zoom ALT-P
![::
    ; save current window ids
    WinGet, active_id, ID, A
    ; find zoom window
    SetTitleMatchMode, 1
    ; toggle zoom
    WinGet, WinID, ID, Zoom
    if (WinID != "") {
        WinActivate, ahk_id %WinID%
        ; toggle mute
        send, {Alt Down}{Alt Up} {Alt Down}{Alt Up}
        send, !a
    }
    ; toggle Jabber
    WinGet, WinID, ID, Conference
    if (WinID != "") {
        
        WinActivate, ahk_id %WinID%
        ; toggle mute
        send, {Control Down}{Down}{Control Up}
    }

    ; reactivate original window
    WinActivate, ahk_id %active_id%
